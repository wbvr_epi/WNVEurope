# Files

* temp_df19802022.csv: Temperature for 5 locations for 1980-2022, see temp_function.R/DATA_temperature.R based on data from: https://surfobs.climate.copernicus.eu/dataaccess/access_eobs.php
* all_runs.csv: Scenarios for remote run of the STAN model
* TESSY_dataNUTS2.csv: Summary of TESSY data by NUTS2 and week. Original source: https://www.ecdc.europa.eu/en/publications-data/request-tessy-data-research
* [folder]: 'maps' Shapefile of EU for NUTS-level mapping. Source: https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts
