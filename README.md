# The effect of temperature on the boundary conditions of West Nile virus circulation in Europe

[09.11.2023] Public repository to the publication titled: **"The effect of temperature on the boundary conditions of West Nile virus circulation in Europe"**

by: Eduardo Costa de Freitas<sup>1</sup>, Kiki Streng<sup>2</sup>, Mariana Avelino de Souza Santos<sup>1</sup>, Michel Jacques Counotte<sup>1</sup><sup>*</sup>

<sup>1</sup>Wageningen Bioveterinary Research, Wageningen University and Research, Lelystad, the
Netherlands

<sup>2</sup>Quantitative Veterinary Epidemiology, Wageningen University and Research, Wageningen, the Netherlands

<sup>*</sup> Corresponding author: E-mail: michel.counotte@wur.nl

[27.10.2023] Figures for publication:

* Figure 1. Results/Figure1.tex Latex compilation into TIKX figure
* Figure 2. RESULTS_R0_1980.R temperature overview
* Figure 3. RESULTS_R0_1980.R R0 heatmap + days R0>1
* Figure 4. RESULTS_bindremoteStan.R Stan model, 5 locations
* Figure 5. RESULTS_bindremoteStan.R Stan model, Utrecht 12 years
* Figure 6. RESULTS_SimInf.R SimInf model, Utrecht 12 years
* Figure S1. RESULTS_SimInf.R SimInf model, 5 locations
* Figure S2. RESULTS_SimInf.R SimInf model, Berlin 12 years

