functions {

   int season(real x, real onset, real duration) { 
		real t;
		int result;
		t = (x-floor(x/365)*365);
		result = t>onset && t<(onset+duration);
		return(result);
	} 

   int int_ceiling(real x) { // real time to integer time https://discourse.mc-stan.org/t/real-to-integer-conversion/5622/9
		int i;
		i = 1;
		//if (x >= 0) 
		while (x > i + 1) i = i + 1;
		//else while (x < i) i = i - 1; //time is always >=0;
		return i;
	} 
	
   real bc_S_fun(real x){
		real q=-3.05E-3;
		real tmin=16.8;
        real tmax=38.9;
		//quadratic(temp,q,tmin,tmax)
		real bc;
		if((x < tmin) || (x > tmax)){
			bc= 0.00001;
		}else{
			bc = q*(x-tmin)*(x-tmax);
		}		
		return bc;
   }
   
   real lf_fun(real x){
   		real lf;
		if(x<(169.8/4.86) && x>=10){
			lf=169.8-4.86*x;
		}else if(x<10){
			lf=120;
		}else{
			lf=0.001;
		}
		return lf;
   }
   
   real a_S_fun(real x){
		real q=1.70E-4;
		real tmin=9.4;
		real tmax=39.6;
		//briere(temp, q, tmin, tmax)		
		real a;
		if((x < tmin) || (x > tmax)){
			a = 0.00001;
		}else{
			a = q*x*(x-tmin)*sqrt(tmax-x);
		}
		return a;
   }
   
   real PDR_S_fun(real x){
		real q=7.38E-5;
		real tmin=11.4;
		real tmax=45.2;
		real k;
		if((x < tmin) || (x > tmax)){
			k = 0.00001;
		}else{
			k = q*x*(x-tmin)*sqrt(tmax-x);
		}
		return k;	
   }
   
   real introduceInf(real x, real y){
		real time = (x-(floor(x/365)*365));
		real n_i;
		
		if(round(time)==y){ // for one day, enter animals: 
			n_i=0.005012542; // 1-exp(r=-0.005); r=-ln(1-x) 5/1000 animals. or 0.5% replaced with I
		}else{
			n_i = 0;
		}
		return n_i;
   }
   
   
  real[] SI(real t, real[] y, real[] theta, 
             real[] x_r, int[] x_i) {

    /*
	## R code:
	seasonR <- season(time, onset, duration)
    temp <- temp_function(time)
    
    k = PDR_S(temp)
    lf = lf_S(temp)
    #NM <- SP+SM+EM+IP+IM
    
    # mosquitoes: active
    dSM <-  -bc_S(temp) * d* a_S(temp) * (IB * SM)/(IB+SB+RB) + seasonR*(SM+EM+IM)*(1/lf) - SM*(1/lf) + seasonR*1/zeta*SP - SM*1/zeta*(1-seasonR)
    dEM <- bc_S(temp) * d* a_S(temp) * (IB * SM)/(IB+SB+RB) - k*EM - EM*(1/lf) - EM*1/zeta*(1-seasonR)#k=0.106
    dIM <- k*EM - IM*(1/lf)+seasonR*1/zeta*IP - IM*1/zeta*(1-seasonR)
    
    # mosquitoes: diapauze/inactive
    dSP <- -seasonR*1/zeta*SP + (SM+EM)*1/zeta*(1-seasonR) - SP*(1/lf)*(1-seasonR) + (1-seasonR)*(SP+IP)*(1/lf)
    dIP <- -seasonR*1/zeta*IP + IM*1/zeta*(1-seasonR) - IP*(1/lf)*(1-seasonR)
    
    # birds
    dSB <- -c* a_S(temp) * d * (IM * SB)/(IB+SB+RB) - SB*(1/bl) + (SB+IB+RB)*(1/bl)
    dIB <- c* a_S(temp) * d * (IM * SB)/(IB+SB+RB) - IB*(1/bl) - g*IB + introduceInf(time, t_intr)*(SB+RB)
    dRB <- g*IB - RB*(1/bl)
    
    # dummy compartments for incidence:
    dCB <- c* a_S(temp) * d* (IM * SB)/(IB+SB+RB)
    
    return(list(c(dSM, dEM, dIM, dSP, dIP, dSB, dIB, dRB, dCB))) 
	*/
	  
	  
	  real SM = y[1]; // Active adult mosquito: Susceptible
	  real EM = y[2]; // Exposed
	  real IM = y[3]; // Infected
	  
	  real SP = y[4]; //diapauzing mosquito
	  real IP = y[5];
	  
	  real SB = y[6]; // birds: Susceptible
	  real IB = y[7]; // birds: Infectious
	  real RB = y[8]; // birds: recovered
	  
	  real CB = y[9]; // birds: dummy compartment: cumulative cases
		
  
      int time_int = int_ceiling(t);
	  int n_days = x_i[1];
	  real temperature[n_days] = x_r[1:n_days];	  
	  real temp = temperature[time_int];
	  
	  // parameters
	  real zeta = theta[1];
	  real onset = theta[2];
	  real duration = theta[3];
	  //real k = theta[4];
	  real bl = theta[4];
	  real d = theta[5];
	  real g = theta[6];
	  real t_intr = theta[7];		
	  real debug = theta[8];
	  
	  // temp-dep/time-dep parameters:
	  real bc_S = bc_S_fun(temp); //vector comptence
	  real seasonR = season(t, onset, duration); //mosquito season yes/no
	  real lf = lf_fun(temp); //lifespan
	  real a_S = a_S_fun(temp); //biting rate  
	  real k = PDR_S_fun(temp); // pathogen development rate (incubation time)
 
	  
	  real dSM_dt =  -bc_S *  a_S * d * (IB * SM)/(IB+SB+RB) + seasonR*(SM+EM+IM)*(1/lf) - SM*(1/lf) + seasonR*1/zeta*SP - SM*1/zeta*(1-seasonR);
	  real dEM_dt = bc_S *  a_S * d * (IB * SM)/(IB+SB+RB) - k*EM - EM*(1/lf) - EM*1/zeta*(1-seasonR);//#k=0.106
      real dIM_dt = k*EM - IM*(1/lf)+seasonR*1/zeta*IP - IM*1/zeta*(1-seasonR);
	  
	  real dSP_dt = -seasonR*1/zeta*SP + (SM+EM)*1/zeta*(1-seasonR) - SP*(1/lf)*(1-seasonR) + (1-seasonR)*(SP+IP)*(1/lf);
      real dIP_dt = -seasonR*1/zeta*IP + IM*1/zeta*(1-seasonR) - IP*(1/lf)*(1-seasonR);
	  
	  real dSB_dt = -0.8* a_S * d * (IM * SB)/(IB+SB+RB) - SB*(1/bl) + (SB+IB+RB)*(1/bl);
      real dIB_dt = 0.8* a_S * d * (IM * SB)/(IB+SB+RB) - IB*(1/bl) - g*IB + introduceInf(t, t_intr)*(SB+RB);
      real dRB_dt = g*IB - RB*(1/bl);	  
	  
	  // dummy compartments for incidence:
	  real dCB_dt = 0.8* a_S * d * (IM * SB)/(IB+SB+RB);
	  
	  // debug:
	  if(debug==1){
		print(t, "=time ", time_int , "=time as integer;", temperature[time_int], "=temp;", a_S, "=a_S");
	  }
	  // print(time_int,": temperature:", temperature[time_int],"photoperiod:",photoperiod[time_int],"; photoperiod ewing:", diap, "cap=",cap, "diap_time=",diap_time);
	  
	  return {dSM_dt, dEM_dt, dIM_dt, dSP_dt, dIP_dt, dSB_dt, dIB_dt, dRB_dt, dCB_dt};
  }
}

data {
  int<lower=1> n_days;
  int<lower=1> n_times;
  //  int n_obs;
  //  int obs_days[n_obs];
  real y0[9];
  real t0;
  real ts[n_times];
  //  int observations[n_obs];
  real temp[n_days]; //1:n_days = temperature, n_days+1:ndays*2 = photoperiod
  int inference;
  real<lower=0> zeta;
  //real<lower=0> onset;
  //real<lower=0> duration;
  //real k;
  real bl;
  //real c;
  real g;
  real t_intr;
  real debug;
  
}


transformed data {
  real x_r[n_days] = temp; //adaptation: pass data through x_r to ODE
  int x_i[1] = { n_days };  //adaptation: pass n_days as integer to ODE
}

parameters {

  //real<lower=0> scaling;
  //real<lower=0> phi_inv;
  //real<lower=0> sigma;
  real<lower=1, upper=300> VH_ratio;
  real<lower=0, upper=1> d;
  real<lower=100, upper=170> onset;
  real<lower=100, upper=200> duration;
}

transformed parameters{
  real y[n_times, 9]; // 9 compartments: 3x mosquito active, 2x diap, 3x bird, 1x dummy C compart.
  //real model_c[n_times];
  real model_c[n_times];
  real model_c_obs[n_times];
  real model_SM[n_times];
  real model_EM[n_times];
  real model_IM[n_times];
  real prop_infM[n_times];
  //real phi = 1. / phi_inv;
  real y0_vh[9];
  /*
  real as[n_times];
  
  for (n in 1:n_times){
	as[n] = a_S_fun(temp[n]);
  }
  */
  
  
  {
  real theta[8]; // parse parameters to ode
  theta[1] = zeta;
	theta[2] = onset;
	theta[3] = duration;
	//theta[4] = k;
	theta[4] = bl;
	theta[5] = d;
	theta[6] = g;
	theta[7] = t_intr;
	theta[8] = debug;

	
	
	y0_vh = y0;
	// correct for VH ratio:
	y0_vh[4] = y0_vh[4]*VH_ratio;
	
	//print("cap=",cap,"diap=",diap_time);
	y = integrate_ode_rk45(SI, y0_vh, t0, ts, theta, x_r, x_i);
	// see: https://mc-stan.org/users/documentation/case-studies/boarding_school_case_study.html#numerical_integrators & https://mc-stan.org/docs/2_23/stan-users-guide/stiff-ode-section.html
	//y = integrate_ode_bdf(SI, y0, t0, ts, theta, x_r, x_i);
	
	model_c = to_array_1d(col(to_matrix(y), 9));
	
	model_IM = to_array_1d(col(to_matrix(y), 3));
	model_EM = to_array_1d(col(to_matrix(y), 2));
	model_SM = to_array_1d(col(to_matrix(y), 1));
	
	
	//model_c_obs = model_c[obs_days];
	
	
	for (n in 1:n_times){
		//model_c_obs[n] = model_c_obs[n] * scaling;
    model_c_obs[n]=(model_c[n]- (n==1 ? 0 : (model_c[n]>model_c[n-1] ? model_c[n-1] : 0) ));
		if(model_EM[n]+model_SM[n]+model_IM[n]>1){
			prop_infM[n] = model_IM[n]/(model_EM[n]+model_SM[n]+model_IM[n]);
		}else{
			prop_infM[n] = 0;
		}
	}
	}
}
model {
  //priors
  //beta ~ normal(20, 3); //truncated at 0
  //onset ~ normal(100, 20);
  //duration ~ normal(100, 20);
  //scaling ~ normal(0,8);
  //phi_inv ~ exponential(5);
  //sigma~cauchy(0,3);
  //VH_ratio~normal(0,100);
  //sampling distribution
  //col(matrix x, int n) - The n-th column of matrix x. Here the number of infected people
  //observations ~ neg_binomial_2(col(to_matrix(y), 2), phi);
  /*
  if(inference==1){
	observations~ neg_binomial_2(model_c, phi);
  }else if(inference==2){
	observations~ poisson(model_c);
  }else if(inference==3){
	sqrt(observations)~normal(sqrt(model_c), sigma);
  }
  */
}

/*
generated quantities {
  //real R0 = beta / gamma;
  //real recovery_time = 1 / gamma;
  real pred_C[n_times];
  real beta_prior;
  real gamma_prior;
  // ergens <0 y?
  if(inference==2){
	for(n in 1:n_times)
		pred_C[n] = poisson_rng(model_c_obs[n] + 1e-5);

  }else{
  for(n in 1:n_times)
     pred_C[n] = neg_binomial_2_rng(model_c_obs[n] + 1e-5, phi);
  }
  
  // added 12.08.2022: have also prior distributions from the model:
  beta_prior=normal_rng(20,3);
  gamma_prior=normal_rng(20,3);
  
}
*/

