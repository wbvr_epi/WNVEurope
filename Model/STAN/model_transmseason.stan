functions {

   int season(real x, real onset, real duration) { 
		real t;
		int result;
		t = (x-floor(x/365)*365);
		result = t>onset && t<(onset+duration);
		return(result);
	} 


	
  real[] SI(real t, real[] y, real[] theta, 
             real[] x_r, int[] x_i) {

      real S = y[1]; // S
      real I = y[2]; // I
	  //real C = y[3]; // C
  
      
	       
	  real beta = theta[1];
	  real onset = theta[2];
	  real duration = theta[3];
	  real seasonR = season(t, onset, duration);
	    
	  //real diap = diap_zeta_Ewing(t, photoperiod[time_int], diap_time);	  
	  
      real dS_dt = -seasonR*1/beta*S + I*1/beta*(1-seasonR);
      real dI_dt =  seasonR*1/beta*S - I*1/beta*(1-seasonR);
	  
	  //real dC_dt = seasonR*1/beta*S;
	  // debug:
      //print(t, "=time ", time_int , "=time as integer;", temperature[time_int], "=temp;", n_days, "=ndays;", C,"=c");
	  // print(time_int,": temperature:", temperature[time_int],"photoperiod:",photoperiod[time_int],"; photoperiod ewing:", diap, "cap=",cap, "diap_time=",diap_time);
	  
      return {dS_dt, dI_dt};
  }
}

data {
  int<lower=1> n_days;
  int<lower=1> n_times;
  int n_obs;
  int obs_days[n_obs];
  real y0[2];
  real t0;
  real ts[n_times];
  int observations[n_obs];
  real temp_photo[n_days]; //1:n_days = temperature, n_days+1:ndays*2 = photoperiod
  int inference;
}


transformed data {
  real x_r[n_days] = temp_photo; //adaptation: pass data through x_r to ODE
  int x_i[1] = { n_days };  //adaptation: pass n_days as integer to ODE
}

parameters {
  real<lower=0> beta;
  real<lower=0> onset;
  real<lower=0> duration;
  real<lower=0> scaling;
  real<lower=0> phi_inv;
  real<lower=0> sigma;
}

transformed parameters{
  real y[n_times, 2]; // 2 compartments: S, I [diapauzing (S), active mosquitoes (I)]
  //real model_c[n_times];
  real model_c[n_obs];
  real model_c_obs[n_times];
  real phi = 1. / phi_inv;
  {
    real theta[3]; // 
    theta[1] = beta;
	theta[2] = onset;
	theta[3] = duration;
	
	//print("cap=",cap,"diap=",diap_time);
	y = integrate_ode_rk45(SI, y0, t0, ts, theta, x_r, x_i);
	// see: https://mc-stan.org/users/documentation/case-studies/boarding_school_case_study.html#numerical_integrators & https://mc-stan.org/docs/2_23/stan-users-guide/stiff-ode-section.html
	//y = integrate_ode_bdf(SI, y0, t0, ts, theta, x_r, x_i);
	// only use the modeled values for the observed days:
	model_c_obs = to_array_1d(col(to_matrix(y), 2));
	//model_c_obs = model_c[obs_days];
	
	
	for (n in 1:n_times)
		model_c_obs[n] = model_c_obs[n] * scaling;
    //  model_c_obs[n]=(model_c_obs[n]- (n==1 ? 0 : (model_c_obs[n]>model_c_obs[n-1] ? model_c_obs[n-1] : 0) ));
	
	model_c = model_c_obs[obs_days];
	for (n in 1:n_obs)
		model_c[n] = model_c[n]<0 ? 1e-5 : model_c[n];
	}
}
model {
  //priors
  beta ~ normal(20, 3); //truncated at 0
  onset ~ normal(120, 20);
  duration ~ normal(120, 20);
  scaling ~ normal(0,8);
  phi_inv ~ exponential(5);
  sigma~cauchy(0,3);
  //sampling distribution
  //col(matrix x, int n) - The n-th column of matrix x. Here the number of infected people
  //observations ~ neg_binomial_2(col(to_matrix(y), 2), phi);
  if(inference==1){
	observations~ neg_binomial_2(model_c, phi);
  }else if(inference==2){
	observations~ poisson(model_c);
  }else if(inference==3){
	sqrt(observations)~normal(sqrt(model_c), sigma);
  }
}

generated quantities {
  //real R0 = beta / gamma;
  //real recovery_time = 1 / gamma;
  real pred_C[n_times];
  real beta_prior;
  real gamma_prior;
  // ergens <0 y?
  if(inference==2){
	for(n in 1:n_times)
		pred_C[n] = poisson_rng(model_c_obs[n] + 1e-5);

  }else{
  for(n in 1:n_times)
     pred_C[n] = neg_binomial_2_rng(model_c_obs[n] + 1e-5, phi);
  }
  
  // added 12.08.2022: have also prior distributions from the model:
  beta_prior=normal_rng(20,3);
  gamma_prior=normal_rng(20,3);
  
}
