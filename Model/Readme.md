# Current model

## Deterministic implementation rstan
* Scripts/STAN/model_transmWNV2.stan: Current Stan model
* Scripts/MODEL_transmWNVstan.R: Runs stan model for scenarios (location/year combinations)
	* Uses: DATA_temperature.R to create temperature dataset
		* Loads: temp_function.R function to extract daily temperature based on location and time (COPERNICUS)
	* Output: according to all_runs.R (combinations of scenarios): Results/WNVrunUyear_1.rds etc.
	* Post-processing: RESULTS_bindremoteStan.R

## Stochastic implementation SimInf
* Scripts/MODEL_transmWNVsimInf_EventIntro.R: Current SimInf model, events functionionality used for introduction of infectious individuals
	* Post-processing: RESULTS_SimInf.R
* MODEL_transmWNVsimInf.R
	
-> Preprocess temperature: DATA_temperature.R
-> Scenarios: Location (5x), Temperature (Years).
-> Data requirement model: Daily temperature, disease parameters.

## Calculation of R0 based on temperature
* RESULTS_R0_1980.R: uses temp_function.R to calculate for each day from the average temperature the R0 value given values for VH ratio/dilution.

## Sensitivity analyses

* Introduction: [stochastic model, 26.07.2023] and effect of stochasticity: dying out? [SimInf implementation?] [26.07.2023 implemented]

# Previous models [not included here]

* Mosquito dynamics model (including eggs, larvae,...); [to be described...]