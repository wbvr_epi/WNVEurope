{
  library(tidyverse)
  library(here) # location files
  library(SimInf)
  library(lubridate)
}
rm(list=ls())
#dir="/home/couno002/Scripts_WNV/"
#dir="C:/Users/couno002/OneDrive - Wageningen University & Research/WNV/Analysis/"
dir="C:/Users/couno002/OneDrive - Wageningen University & Research/WNV/GitLab/"

df = read.csv(paste0(dir,"Data/temp_df19802022.csv")) %>% 
  mutate(date=as.Date(date), year=year(date)) %>%
  filter(year>2009)
# 
# all_runs=df  %>%filter(location=="Utrecht") %>% #%>% filter(year==2020)
#   group_by(location,year) %>%
#   summarize() %>%
#   ungroup() %>%
#   mutate(id=1:n(), start=NA)

# check which scenarios to run:
all_runs = read.csv(paste0(dir,"Data/all_runs.csv"))

next_run = 1
#next_run= which(is.na(all_runs$start))[1]
#all_runs$start[next_run] = as.character(Sys.time())
# # write which was run:
#write.csv(file=paste0(dir,"all_runs.csv"), row.names=FALSE, all_runs)
# 
year_run=all_runs$year[next_run]
location_run=all_runs$location[next_run]
location_run="Berlin"
id_run=all_runs$id[next_run]

this_df=df %>% filter(year==year_run,location==location_run)

time=1:nrow(this_df)
temp=this_df$temp

y0 = c(SM = 1000, EM = 0, IM = 0, SB = 1000, IB = 0, RB = 0) #, CB = 0
n=1000 # number of replications
transitions <- c("@ -> (SM+EM+IM)*(1/lf)  -> SM",
                 "SM -> (1/lf)*SM -> @",# 
                 "SM -> bc_S * a_S * d * (IB * SM)/(IB+SB+RB) -> EM",
                 "EM -> (1/lf)*EM -> @", 
                 "EM -> k*EM -> IM",
                 "IM -> (1/lf)*IM -> @",
                 "SB -> 0.8* d* a_S * (IM * SB)/(IB+SB+RB) -> IB", 
                 "@ -> (SB+IB+RB)*(1/bl) -> SB",
                 "SB -> (1/bl)*SB -> @", 
                 "@ -> introduceInf*(SB+RB) -> IB",
                 "IB -> g*IB -> RB",
                 "IB -> (1/bl)*IB  -> @",
                 "RB -> (1/bl)*RB -> @")

compartments <- c("SM", "EM", "IM", "SB", "IB", "RB")
# size of compartments (n indivuals)
u0 = data.frame(t(y0))%>% slice(rep(1:n(), each = n))
tspan = 1:(1*365) # lets take 3 year2
temp_select=temp[tspan]

# ldata=data.frame(#EFD_own=rep(0.1,n*n2),
#                  cap=rep(0.005,n*n2))
#                  #MDR_S=rep(0.1, n*n2),
#                  #lf_S=rep(100, n*n2),
#                  #diap_zeta_Ewing=rep(0.01, n*n2),
#                  #tspan=max(tspan)) #birth=rep(0,n*n2)

v0 = data.frame(lf=rep(70,n),      # vnew[0]
                bc_S = rep(0.3,n), # vnew[1]
                a_S = rep(0.3,n),  # vnew[2]
                d = rep(0.5,n),
                k = rep(0.2,n),    # vnew[4]
                bl = rep(365,n),
                g = rep(0.2,n),
                introduceInf=rep(0.00001,n)) # vnew[7]

result_df = list()
intrs = c(100 + (1:10*10))

years_all = unique(df$year)

# run scenarios with different introduction times, vh_Ratios and years. Temperature is based on daily mean temperature.
scenarios = expand.grid(intrs=intrs, vh_ratio=2^(1:6), year=years_all[4:13], n_birds=c(1000))

for(i in 1:nrow(scenarios)){
  
  
  #intr_time = 150
  intr_time = scenarios$intrs[i]
  VH_ratio = scenarios$vh_ratio[i]
  n_birds = scenarios$n_birds[i]
  y0['SB'] = n_birds
  y0['SM'] = y0['SB'] * VH_ratio
  year_run = scenarios$year[i]
  
  
  this_df=df %>% filter(year==year_run,location==location_run)
  time=1:nrow(this_df)
  temp=this_df$temp
  
  u0 = data.frame(t(y0))%>% slice(rep(1:n(), each = n))
  
  cat("Scenario",i, "intr_time =",intr_time, "VH_ratio =", VH_ratio, "year =", year_run,  "\n")
  
  intr_rate = 0.005012542 
  
  # n=(1-exp(-0.005012542))*1000
  # 
  ldata = data.frame(t(c(intr_time,intr_rate,temp))) %>% slice(rep(1:n(), each = n))
  
  model <- mparse(transitions = transitions, 
                  compartments = compartments, 
                  u0=u0, 
                  tspan  = tspan,#events = events,
                  v0=v0,
                  ldata=ldata)
  #writeLines(model@C_code, paste0(dir,"Model/Custom_C/custom_model.c"))
  
  model@C_code <- readLines(paste0(dir,"Model/Custom_C/custom_modeltemp.c"))
  result <- run(model, threads = 1)
  intr_date = (as.Date(paste0(year_run,"-01-01"))+intr_time)
  
  traject <- trajectory(result) %>% 
    mutate(date=as.Date(paste0(year_run,"-01-01"))+time,
           intr_date = intr_date,
           intr_time = intr_time,
           VH_ratio = VH_ratio,
           year = year_run, 
           n_birds = n_birds)
  
  summary_df = traject %>% group_by(intr_time, node, VH_ratio, year) %>%
    summarize(max_B = max(RB)/max(RB+IB+SB)) #%>%
  
  result_df[[i]] = traject
}
traject_all=result_df %>% bind_rows()

# traject_all %>% filter(node<11) %>%
#   ggplot(aes(x=date))+
#   geom_line(aes(y=IM, color="Mosquitoes"))+
#   geom_line(aes(y=IB, color="Birds"))+
#   theme(legend.position = "bottom")+
#   facet_wrap(~node)

tmp=traject_all %>% group_by(intr_time, VH_ratio, year) %>%
  summarize(max_B = max(RB), max_M = max(IM)) 

tmp=traject_all %>% filter(time==intr_time+1) %>%
  group_by(intr_time, VH_ratio, year, n_birds) %>%
  summarize(m=median(IB)) %>%
  ggplot()+
  geom_point(aes(intr_time, m))

traject_all %>% group_by(intr_time, node, VH_ratio, year, n_birds) %>%
  summarize(max_B = max(RB)/max(RB+SB+IB)) %>%
  group_by(intr_time, VH_ratio, year, n_birds) %>%
  summarize(prop=sum((max_B>0.1))/n()) %>%
  ggplot()+
  geom_tile(aes(intr_time,factor(VH_ratio), fill=prop))+
  facet_grid(col=vars(year),rows=vars(n_birds))

traject_all %>% group_by(date,intr_date, year) %>%
  mutate(IM = IM/(IM+SM+EM)) %>%
  summarize(IM_med=median(IM), IM_low=quantile(IM,0.025), IM_high=quantile(IM,0.975)) %>%
  ggplot(aes(date,IM_med))+
  geom_vline(aes(xintercept=intr_date), color="red")+
  geom_line()+
  geom_ribbon(aes(ymin=IM_low, ymax=IM_high), alpha=0.5)+
  facet_wrap(~intr_date)
